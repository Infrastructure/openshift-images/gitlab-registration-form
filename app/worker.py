import json
import socket
import uuid
from email.utils import parseaddr
from typing import Optional

import dramatiq
import dramatiq.brokers.redis
import gitlab
import gitlab.exceptions
import httpx
import psycopg
import sentry_sdk
from anthropic import Anthropic
from sentry_sdk.integrations.dramatiq import DramatiqIntegration
from settings import settings
from tenacity import retry, stop_after_attempt, wait_random
from ua_parser import user_agent_parser

if settings.sentry_dsn:
    sentry_sdk.init(
        dsn=settings.sentry_dsn,
        environment=settings.environment,
        integrations=[
            DramatiqIntegration(),
        ],
    )

broker = dramatiq.brokers.redis.RedisBroker(
    host=settings.redis_host, port=settings.redis_port, db=0
)
dramatiq.set_broker(broker)


def verify_captcha(token: str, ip: str):
    uuid_namespace = uuid.uuid5(uuid.NAMESPACE_DNS, "register.gitlab.gnome.org")
    captcha_uuid = uuid.uuid5(uuid_namespace, token)

    data = {
        "secret": settings.captcha_secret,
        "response": token,
        "remoteip": ip,
        "idempotency_key": str(captcha_uuid),
    }

    response = httpx.post(
        "https://challenges.cloudflare.com/turnstile/v0/siteverify", json=data
    )
    response.raise_for_status()
    return response.json()


def verify_email(email: str):
    apikey = settings.ipqualityscore_key
    response = httpx.get(f"https://ipqualityscore.com/api/json/email/{apikey}/{email}")
    response.raise_for_status()
    return response.json()


@retry(stop=stop_after_attempt(5), wait=wait_random(min=1, max=5))
def _call_ipapi(ip: str, apikey: str) -> dict:
    response = httpx.get(f"https://api.ipapi.is?q={ip}&key={apikey}")
    response.raise_for_status()
    return response.json()


def is_anon(request):
    anon_fields = [
        "is_crawler",
        "is_tor",
        "is_datacenter",
        "is_vpn",
        "is_proxy",
    ]
    for field in anon_fields:
        if request.get(field):
            return True


def check_dronebl(ip: str) -> bool:
    octets = ip.split(".")
    reversed_ip = ".".join(reversed(octets))

    query = f"{reversed_ip}.dnsbl.dronebl.org"

    try:
        socket.gethostbyname(query)
        return True
    except socket.gaierror:
        return False


def verify_ip(ip: str, useragent_str: Optional[str]) -> tuple[bool, Optional[dict]]:
    response = None

    if check_dronebl(ip):
        return False, {"dronebl_listed": True}

    try:
        apikey = settings.ipapi_key
        response = _call_ipapi(ip, apikey)

        if not response:
            return True, None

        if response.get("is_abuser"):
            return False, response

        useragent = user_agent_parser.Parse(useragent_str)
        useragent_os = useragent.get("os", {}).get("family")

        if useragent_os != "Linux":
            if abuser_score := response.get("abuser_score"):
                if "(High)" in abuser_score:
                    return False, response

            if is_anon(response):
                return False, response

            location = response.get("location", {})
            if country_code := location.get("country_code"):
                if country_code in (
                    "BD",
                    "CN",
                    "EG",
                    "ID",
                    "IN",
                    "KH",
                    "NP",
                    "PH",
                    "PK",
                    "TR",
                    "VN",
                ):
                    return False, response

    except (httpx.ReadTimeout, httpx.RequestError):
        return True, None

    return True, response


def create_user(name: str, username: str, email: str):
    gl = gitlab.Gitlab("https://gitlab.gnome.org", private_token=settings.gitlab_token)
    user = gl.users.create(
        {
            "name": name,
            "username": username,
            "email": email,
            "skip_confirmation": True,
            "random_password": True,
            "reset_password": True,
        }
    )
    return user


def normalize_email(email: str) -> str:
    email = email.lower()

    name, domain = parseaddr(email)[1].split("@")
    if domain in ("gmail.com", "googlemail.com"):
        name = name.split("+")[0].replace(".", "")
        return f"{name}@{domain}"

    return email


def is_blacklisted_email(email: str) -> bool:
    blacklisted_domains = {
        "the30top.com",
    }
    _, domain = parseaddr(email)[1].split("@")
    return domain.lower() in blacklisted_domains


@dramatiq.actor
def process_form(
    reg_id: str,
    name: str,
    username: str,
    email: str,
    client_ip: str,
    user_agent: Optional[str],
    captcha: str,
):
    reason = None
    email_response = None
    captcha_response = None
    ipapi_response = None

    if is_blacklisted_email(email):
        reason = "blacklisted_domain"

    if not reason:
        captcha_response = verify_captcha(captcha, client_ip)
        if not captcha_response.get("success"):
            if captcha_response.get("error-codes"):
                if "timeout-or-duplicate" not in captcha_response["error-codes"]:
                    reason = "captcha"

    if not reason:
        ip_verdict, ipapi_response = verify_ip(client_ip, user_agent)
        if not ip_verdict:
            reason = "likely_spammer"

    if not reason:
        try:
            email_response = verify_email(email)
            if email_response.get("success"):
                if email_response.get("disposable"):
                    reason = "disposable_email"
        except httpx.ReadTimeout:
            pass

    if not reason:
        try:
            create_user(name, username, email)
        except gitlab.exceptions.GitlabCreateError as e:
            if e.response_code == 409:
                if e.error_message == "Email has already been taken":
                    reason = "email_conflict"
                elif e.error_message == "Username has already been taken":
                    reason = "username_conflict"
                else:
                    raise
            else:
                raise
        except gitlab.exceptions.GitlabHttpError as e:
            if e.response_code == 400 and "username" in str(e):
                reason = "username_invalid"
            else:
                raise

    action = "reject" if reason else "accept"

    with psycopg.connect(settings.database_url) as conn:
        with conn.cursor() as cur:
            cur.execute(
                "INSERT INTO audit_log (id, username, email, action, reason, ipqs_email_response, captcha_response, user_agent, ipapi_response) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                (
                    reg_id,
                    username,
                    normalize_email(email),
                    action,
                    reason,
                    json.dumps(email_response),
                    json.dumps(captcha_response),
                    user_agent,
                    json.dumps(ipapi_response),
                ),
            )
            conn.commit()


@dramatiq.actor
def process_account_request(iid: int):
    gl = gitlab.Gitlab("https://gitlab.gnome.org", private_token=settings.gitlab_token)
    project = gl.projects.get(31221)
    issue = project.issues.get(iid)
    email = normalize_email(issue.service_desk_reply_to)
    reason = "unknown"

    with psycopg.connect(settings.database_url) as conn:
        with conn.cursor() as cur:
            cur.execute(
                "SELECT reason, username, ipqs_email_response, captcha_response, user_agent, ipapi_response FROM audit_log WHERE email = %s AND action != 'accept'",
                (email,),
            )
            row = cur.fetchone()

    if row:
        (
            reason,
            username,
            ipqs_email_response,
            captcha_response,
            user_agent,
            ipapi_response,
        ) = row

        registration_data = f"""
Rejection reason: `{reason}`

User-Agent: `{user_agent}`

IPQS e-mail check:
```json
{json.dumps(json.loads(ipqs_email_response) if ipqs_email_response else None, indent=2)}
```

Captcha response:
```json
{json.dumps(json.loads(captcha_response) if captcha_response else None, indent=2)}
```

ipapi.is:
```json
{json.dumps(json.loads(ipapi_response) if ipapi_response else None, indent=2)}
```
"""

        issue.notes.create({"body": registration_data, "confidential": True})

        system_prompt = """
        You are a helpful assistant evaluating GitLab registration requests for the GNOME project. Users reaching out through this process are likely genuine contributors who encountered technical issues during registration.

        Consider the rejection reason in context:
        - disposable_email: The user may be privacy-conscious or using a temporary email for initial contact
        - email_conflict: Could be a forgotten account or confusion about registration status
        - username_conflict: The desired username may already be taken
        - captcha: Likely technical issues with verification
        - likely_spammer: Could be using a VPN, accessing from a restricted network, listed in DroneBL blocklist, or has a high abuser score
        - unknown: No specific technical issues identified

        Focus on the user's stated intentions and interest in GNOME. Evaluate whether they demonstrate genuine interest and understanding of the project.

        IMPORTANT: Only approve requests where the user clearly explains why they want to contribute to GNOME.
        Requests without proper rationale or clear intentions MUST be rejected for manual review.
        Generic requests like "Please create account" should be rejected.

        Respond with exactly two lines:
        Line 1: Your analysis of their request (1-2 sentences)
        Line 2: Either "APPROVE" or "REJECT" (just the single word)
        """

        client = Anthropic(api_key=settings.anthropic_api_key)
        response = client.messages.create(
            model="claude-3-haiku-20240307",
            max_tokens=1024,
            system=system_prompt,
            messages=[
                {
                    "role": "user",
                    "content": f"""
                        Rejection reason: `{reason}`

                        User's explanation:
                        {issue.description}
                     """,
                }
            ],
        )

        analysis = response.content[0].text
        issue.notes.create({"body": analysis, "confidential": True})
        decision = analysis.strip().split("\n")[-1].strip()

        if decision == "APPROVE":
            try:
                create_user(username, username, email)
                issue.state_event = "close"
                issue.save()
                issue.notes.create(
                    {
                        "body": "Account automatically created based on review.",
                        "confidential": True,
                    }
                )
            except gitlab.exceptions.GitlabCreateError as e:
                error_details = (
                    json.dumps(e.error_message, indent=2)
                    if isinstance(e.error_message, dict)
                    else e.error_message
                )
                issue.notes.create(
                    {
                        "body": f"Failed to create account for username '{username}' and email '{email}':\n\n```json\n{error_details}\n```\n\nResponse code: {e.response_code}",
                        "confidential": True,
                    }
                )
    else:
        issue.notes.create(
            {
                "body": "No previous registration attempt found. Manual review required.",
                "confidential": True,
            }
        )


@dramatiq.actor
def create_account(iid: int):
    gl = gitlab.Gitlab("https://gitlab.gnome.org", private_token=settings.gitlab_token)
    project = gl.projects.get(31221)
    issue = project.issues.get(iid)
    email = issue.service_desk_reply_to
    normalized_email = normalize_email(email)
    username = None

    with psycopg.connect(settings.database_url) as conn:
        with conn.cursor() as cur:
            cur.execute(
                "SELECT username FROM audit_log WHERE email = %s AND action != 'accept'",
                (normalized_email,),
            )
            row = cur.fetchone()

    if row:
        username = row[0]

    if not username:
        system_prompt = """
            You are helping extract username from GitLab registration request that was initially rejected.
            The username is required to create the account.

            GitLab username limitations:
            - Maximum length is 255 characters
            - Can only include non-accented letters, digits, '_', '-', and '.'
            - Cannot start with '-', '_', or '.'
            - Cannot end with '-', '_', '.', '.git', or '.atom'

            If you find a requested username that violates these rules, suggest a valid alternative by:
            1. Removing invalid characters
            2. Ensuring it starts and ends with valid characters
            3. Shortening if necessary

            Return only the adjusted username.

            If you can find a clear username request in the text, return only that username (adjusted if needed).
            If you cannot find a clear username request, or if you're unsure, return exactly 'None'.

            Examples:
            - "I would like username johnsmith123" -> johnsmith123
            - "My preferred username is dev.jane" -> dev.jane
            - "I want username .invalid-name.git" -> invalid-name (removed invalid start/end characters)
            - "Please create my account" -> None
            - "I want to contribute to GNOME" -> None
            """

        client = Anthropic(api_key=settings.anthropic_api_key)
        response = client.messages.create(
            model="claude-3-haiku-20240307",
            max_tokens=1024,
            system=system_prompt,
            messages=[
                {
                    "role": "user",
                    "content": f"""
                        {issue.description}
                    """,
                }
            ],
        )
        username = response.content[0].text

    if not username or username.lower() == "none":
        issue.notes.create(
            {
                "body": "Could not automatically determine the requested username. Please ask the user to specify their desired username.",
                "confidential": True,
            }
        )
        return

    try:
        create_user(username, username, email)
        issue.state_event = "close"
        issue.save()
    except gitlab.exceptions.GitlabCreateError as e:
        error_details = (
            json.dumps(e.error_message, indent=2)
            if isinstance(e.error_message, dict)
            else e.error_message
        )
        issue.notes.create(
            {
                "body": f"Failed to create account for username '{username}' and email '{email}':\n\n```json\n{error_details}\n```\n\nResponse code: {e.response_code}",
                "confidential": True,
            }
        )

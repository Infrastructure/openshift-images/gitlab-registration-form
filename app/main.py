import os
import uuid
from typing import Optional

import psycopg_pool
import sentry_sdk
import worker
from fastapi import FastAPI, Form, Request, status, Header, HTTPException
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from settings import settings
import hmac

if settings.sentry_dsn:
    sentry_sdk.init(
        dsn=settings.sentry_dsn,
        environment=settings.environment,
        traces_sample_rate=1.0,
        profiles_sample_rate=1.0,
    )

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
root = os.path.dirname(os.path.abspath(__file__))
templates = Jinja2Templates(directory="templates")
dbpool = psycopg_pool.AsyncConnectionPool(settings.database_url, max_size=5)


@app.get("/")
def main(request: Request):
    return templates.TemplateResponse(request=request, name="index.html.j2", context={})


@app.post("/sign_up")
async def sign_up(
    request: Request,
    name: str = Form(...),
    username: str = Form(...),
    email: str = Form(...),
    captcha: Optional[str] = Form(None, alias="cf-turnstile-response"),
):
    reg_id = str(uuid.uuid4())
    client_ip = request.client.host
    user_agent = request.headers.get("User-Agent")

    if not captcha:
        return templates.TemplateResponse(
            request=request, name="rejected.html.j2", context={"reason": "captcha"}
        )

    worker.process_form.send(
        reg_id,
        name,
        username,
        email,
        client_ip,
        user_agent,
        captcha,
    )

    return RedirectResponse(f"/sign_up/{reg_id}", status_code=status.HTTP_303_SEE_OTHER)


@app.get("/sign_up/{reg_id}", response_class=HTMLResponse)
async def almost_there(request: Request, reg_id: str):
    template_file = None
    context = {}

    async with dbpool.connection() as conn:
        async with conn.cursor() as cur:
            await cur.execute(
                "SELECT action, reason FROM audit_log WHERE id = %s",
                (reg_id,),
            )
            row = await cur.fetchone()

    if not row:
        template_file = "in_progress.html.j2"
    else:
        action, reason = row
        match action:
            case "accept":
                template_file = "almost_there.html.j2"
            case "reject":
                template_file = "rejected.html.j2"
                context["reason"] = reason
            case _:
                template_file = "in_progress.html.j2"

    return templates.TemplateResponse(
        request=request, name=template_file, context=context
    )


@app.post("/api/webhook")
async def handle_webhook(
    request: Request,
    x_gitlab_token: str = Header(...),
):
    if not hmac.compare_digest(x_gitlab_token, settings.webhook_secret):
        raise HTTPException(status_code=403)

    payload = await request.json()
    object_kind = payload["object_kind"]

    match object_kind:
        case "issue":
            iid = payload["object_attributes"]["iid"]
            if payload["object_attributes"]["action"] == "open":
                worker.process_account_request(iid)
        case "emoji":
            if payload["event_type"] == "award":
                if payload["object_attributes"]["name"] == "thumbsup":
                    iid = payload["issue"]["iid"]
                    worker.create_account(iid)

    return {"status": "ok"}

CREATE TABLE IF NOT EXISTS audit_log (
	id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
	username VARCHAR(255),
	email VARCHAR(255),
	action VARCHAR(255),
	reason VARCHAR(255),
	ipqs_email_response TEXT,
	captcha_response TEXT,
	user_agent VARCHAR(255),
	ipapi_response TEXT
);

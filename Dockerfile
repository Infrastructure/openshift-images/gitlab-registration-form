FROM registry.access.redhat.com/ubi9/python-311:1

EXPOSE 8000
ENV PYTHONPATH=/opt/app-root/src

USER 0
ADD app /tmp/src
ADD requirements.txt /tmp/src
RUN /usr/bin/fix-permissions /tmp/src

USER 1001
RUN /usr/libexec/s2i/assemble

CMD uvicorn main:app --host 0.0.0.0 --port 8000 --forwarded-allow-ips '*'

from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    redis_host: str = "redis"
    redis_port: int = 6379
    ipqualityscore_key: str
    captcha_secret: str
    gitlab_token: str
    database_url: str
    ipapi_key: str
    webhook_secret: str
    anthropic_api_key: str
    sentry_dsn: str | None = None
    environment: str = "production"


settings = Settings()
